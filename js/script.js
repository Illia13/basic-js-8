"use strict";

//! 1. Створіть масив з рядків
//! "travel", "hello", "eat", "ski", "lift" та обчисліть кількість рядків з довжиною більше за 3 символи. Вивести це число в консоль.

// let arr = ["travel", "hello", "eat", "ski", "lift"];
// let arrLength = arr.filter((str) => str.length > 3).length;
// console.log(arrLength);

//! 2. Створіть масив із 4 об'єктів, які містять інформацію про людей: {name: "Іван", age: 25, sex: "чоловіча"}.
//! Наповніть різними даними. Відфільтруйте його, щоб отримати тільки об'єкти зі sex "чоловіча"
//! Відфільтрований масив виведіть в консоль.

// let arr = [
//   { name: "Іван", age: 25, sex: "чоловіча" },
//   { name: "Марія", age: 30, sex: "жіноча" },
//   { name: "Олександр", age: 22, sex: "чоловіча" },
// ];
// let arrPeople = arr.filter((person) => person.sex === "чоловіча");
// console.log(arrPeople);

//! 3. Реалізувати функцію фільтру масиву за вказаним типом даних. (Опціональне завдання)
function filterBy(array, dataType) {
  return array.filter((item) => typeof item !== dataType);
}

let arr = ["hello", "world", 23, "23", null];
let arrFilter = filterBy(arr, "string");
console.log(arrFilter);
